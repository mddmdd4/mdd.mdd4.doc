#MDD4 online manual#
##Introduction##
This online manual should give you all the answers you have regarding the MDD4 application.  
**Remark:** There might be chapters in this documentation which you will not see in your application. This is based on the privilges you have given. So don't worry, everything is all right.
Netherless, if you like to get access to one of this part of the application, please contact MDD.
<admin@mdd.ch>
##Video examples##
This are the different methodes for video integration.

###Youtube video embeded###
<iframe width="640" height="360" src="https://www.youtube.com/embed/YIjWVAh42Vk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<p><a href="http://www.youtube.com/watch?v=YIjWVAh42Vk">XBRL explained in one minute</a> from <a href="https://www.youtube.com/channel/UCTuizq6ykmvV2ne1etv_SlQ">DeloitteNederland</a> on <a href="https://www.youtube.com/">Youtube</a>.</p>

###Vimeo video with image sizing###
<iframe src="https://player.vimeo.com/video/125436353?color=6fbbec&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/125436353">XBRL Reports animation</a> from <a href="https://vimeo.com/dirkjanhaarsma">Dirk Jan Haarsma</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

##Getting started##
**Remark:** The following browsers are suported: IExxx, Edge, Chrome.   
Get started now with MDD4. Just go to the following site: [MDD4](https://four.mdd.ch "MDD4")  
Please enter your name and password:  
![Login-Screen1](https://drive.google.com/uc?id=1z72c28IMHb9a1J3d-Jb82YCjb3hWoIK4)

In a view seconds you should receive your code. The code could be sent by Flash-SMS or by email. This depends on the companies preference.
![Login-Screen2](https://drive.google.com/uc?id=1D7_SlUyFX-lZW4RJlUhSO7k0Yh39ix-e)

Congratulations: you are ready to go!
##Switch project##
Your company might have different projects runnung. Please make sure, that you are working on the right project.
The selected project is always visible at the top right side of the screen:
Picture!
##Settings##
This chapter is for administrator users only! Please pay attention, you can break the system by wrong configuration.
###default_variableSet###
Definition file: variableSet.xml  
Description: tbd
###Al-config###
Definition file: al-config.zip  
Description: this .zip file contains information for the Indesign-PDF generation process. Maintained by MDD stuff only.
###ProductionPDFConfig###
Definition file: PdfProof.xml  
Description: tbd
###frPub_dictionary###
Definition file: dictionary.xml  
Description: tbd
###SynchronizeContentFromLayoutCleaner###
Definition file:SynchronizeContentFromLayout.xml  
Description: tbd
###TableVariables##
text
###Language###
Definition file: language.json  
Description: all the languages within this JSON file will be available within the project.  
Definition of a language:  

- "code": "ISO-code"
- "label": "text within UI"
- "shortLabel": "xy"  

As an example the definition of spanish looks like this:  

- "code": "es"
- "label": "Spanish"
- "shortLabel": "ES"  

In addition the default language will be defined as well within this configuration file.

###FinancialData###
Definition file: financialData.json  
Description: this is the definition of the different import files from the menue: "Financial data / Import facts".
###TextProceduresColors###
Definition file: TextProcedureColoers.xml  
Description: tbd
###Variables###
Definition file: variables.json  
Description: in this table you can define variables which will then be replaced in the output.  
Definition of a variable: 

- key: "Issuer-name"
- value: "My-company-name"

**Remark:** There are two standard variables defined: "LY" - last year, "CY" - current year.  
Example:  

- key: "Business year"
- value: "#{CY}" 

##Generic styles##
text
##Manage projects##
In this section you can creat new projects and as well new reports which will be part of a project.
###Projects###
To creat a new project select the button ***ADD PROJECT***
![ADD-PROJECT](https://drive.google.com/uc?id=1WNsSRQbD0W4gYFcTULsB9_sMtFyQxKxE)

You need to enter a project date.
In addition you can upload your taxonomy for this project. (needs to be XBRL compliant)

####Copy project / RollForward project####
Copy the project and define the new date. All the containing reports are getting updated with the new date information of the project. A report will in the new project only be visible, when the frequency of the report is aligned with the project date. Because this functionallity is a crucial part of the system, we will explain it in more detail.

To copy a project select the menu with the three dots and then ***Copy project***
![ADD-REPORT](https://drive.google.com/uc?id=1SZqHwDbYzYUBq2LpmhAp8EyAgLHEgovG)

In the bottom part of the screen you need to define the date of the project. The project with the new date gets created. Be aware, only reports with aligned frequency will be visible.

####Rollforward examples for different cases####

Example 1: Equal report every quarter
![ADD-REPORT](https://drive.google.com/uc?id=1nUVqHL6B-MYSpvk1VbBpHpMLmZPP54yB)

Steps to do:

- create the first project including a report with frequency quarter
- copy the project with the date for the next quarter report
- a new project including the report gets created
- just modify the report, date are already updated
- for next quarter just copy the project again 

Example 2: Different EndYear (EY) and HalfYear (HY) report
![ADD-REPORT](https://drive.google.com/uc?id=1xnkZXpLDiqe7ATGFy78AA3FhkHy_-rCj)

Steps to do:

- create the first project including a report with frequency annual
- copy the project with the date for the next half year report
- a new project gets created, only the content on project level gets copied
- create a new report for the half year (frequency half year)
- copy the project again for the next end year
- the project gets created with the last end year report and as well the last half year report
- just modify the end year report, dates are already updated

Example 3: Different reports for every quarter
![ADD-REPORT](https://drive.google.com/uc?id=1POSHM5A1T21zP_8QNk_n9jYqcHo_KS2Q)

Steps to do:

- create the first project including a report with frequency annual
- copy the project with the date for the next quarterly report
- a new project gets created, only the content on project level gets copied
- create a new report for the quarterly report (frequency quarter)
- copy the project again for the next half year
- the project gets created with the project content and the quarterly report
- create a new report for the half year (frequency semi)
- copy the project again for the third quarter report
- the project gets created with the project content and the last quarter and half year report
- modify the quarter report for the 3rd quarter
- copy the project again for the end year report
- the project gets created with all the reports (including last end year)
- modify the end year report, all the dates are already updated
- move on 

**Remark:** Content which needs to be available for every project, should always be on a project level.

###Reports###
A Report is part of a project. A Project can contain sevral reports.
To creat a new report select the button ***ADD REPORT***
![ADD-REPORT](https://drive.google.com/uc?id=1MnMV07jeQLKEuVcQ3CHjxsVHxxqUSTkh)

A report is defined by the **Report name**.
In addition you need to define the **Frequency** of a report. This is important if you later on copy the project to another date.
The following options are available:

- By month
- By quarter
- By semester
- Annual 

**Remark:** A RollForward works only for a project and not a single report.

##Content&Outputs##
text

###Select the right Period###
When building up a new table the most important part is to select the correct period for a single value or for the whole table.
The following picture shows the whole list of the predefined periods.
![Available-Periods](https://drive.google.com/uc?id=133eUC-piF-VMRGSPN4Yx38GqavasjPa1)
**Remark:** The date of the project for this period selection is December 2016.
What do these differnt piriods mean:
####Current Period (4Q16)####
This period always adapt with a rollforward to the current period.
####Previous Period (3Q16)####
This period always adapt with a rollforward to the previous period.
####Current Period from Last Year (4Q15)####
This period always adapt with a rollforward to the current period from a year before.
####Previous Period from Last Year (3Q15)####
This period always adapt with a rollforward to the previous period from a year before.
####Current Month (12M16)####
This selection shows always the current month.
####Previous Month (12M15)####
This selection shows always the previous month.
####Current Month from Last Year (12M15)####
This selection shows always the current month from a year before.
####Current Semester (12MQ16)####
This selection shows always the current semester (half year).
####Previous Semester (6M16)####
This selection shows always the previous semester.
####Current Semester from Last Year (12M15)####
This selection shows always the current semester from a year before.
####End of Current Year (2016)####
This selection shows always the current year. Always end of year.
####End of Previous Year (2015)####
This selection shows always the previous year. Always end of year.
####End of Two Years Ago (2014)####
This selection shows always the period 2 years ago. Always end of year.

text
##Layout##
text
##Report structure##
In this part you define the structure of your report. In other words you build up your chapters, notes or sections of your actual report.
Start this by selecting the button ***STRUCTURE***
![STRUCTURE](https://drive.google.com/uc?id=1Yydwje32AgWC94HLrW17GO1QFRaF3Y-V)

As a next step you can start adding chapters to your report.
Select the *arrow* sign to creat the first chapter.
![ADD-FIRST-CHPATER](https://drive.google.com/uc?id=1c1PT0oMRZaLnBRhpAnsv9FidjG6FBksL)

Adding new chapters:

- straight arrow:   new chapter on same level
- angled arrow:     new sub-chapter

![ADD-FIRST-CHPATER](https://drive.google.com/uc?id=1NC7vN0wu_y4CQE-Y33O-LQP6-6M6w0be)

Whenever you are finished with your structure you need to publish the structure.
Press the orange Button ***PUBLISH***. With ***DISCARD*** you will loose all changes.
![ADD-FIRST-CHPATER](https://drive.google.com/uc?id=1ZM93NKC_drkH88JOqxYRWVe2OPLIWfbu)

Whenever you need to change your structure you just come back and start modifying it.

**Remark:** Never forget to PUBLISH at the end.


##Translations##
text
##Financial data##
Within the Financial Data are you can build up your specific taxonomy. 
###Mapping instructions###
text
###Import facts###
text
###Validations###
text
#Learning videos#
Finde in this chapter learning videos about different topics of MDD4.
#FAQs#
###Download of files does not work###
Please check if you do have pop-ups enabled for the MDD4 website.
Finde more information for:  
[Google Chrome](https://support.google.com/chrome/answer/95472?co=GENIE.Platform%3DDesktop&hl=en)  
[Microsoft IE11](https://support.microsoft.com/en-us/help/17479/windows-internet-explorer-11-change-security-privacy-settings)  
[Microsoft Edge](https://support.microsoft.com/en-us/help/4026392/windows-block-pop-ups-in-microsoft-edge)

###Titel###
text

##Feedback##
If you have any question or feedback regarding MDD4. Please contact us:
<info@mdd.ch>
---